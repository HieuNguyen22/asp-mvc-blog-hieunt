﻿namespace asp_mvc_blog_hieunt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blogs", "title", c => c.String(maxLength: 30));
            AlterColumn("dbo.Blogs", "shortDetail", c => c.String(maxLength: 60));
            AlterColumn("dbo.Blogs", "detail", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Blogs", "detail", c => c.String());
            AlterColumn("dbo.Blogs", "shortDetail", c => c.String());
            AlterColumn("dbo.Blogs", "title", c => c.String());
        }
    }
}
