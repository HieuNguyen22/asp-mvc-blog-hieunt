﻿namespace asp_mvc_blog_hieunt.Migrations
{
    using asp_mvc_blog_hieunt.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.UI.WebControls;

    internal sealed class Configuration : DbMigrationsConfiguration<asp_mvc_blog_hieunt.Models.BlogDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "asp_mvc_blog_hieunt.Models.BlogDBContext";
        }

        protected override void Seed(asp_mvc_blog_hieunt.Models.BlogDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Blogs.AddOrUpdate(i => i.title,
                new Blog
                {
                    title = "When Harry Met Sally",
                    shortDetail = "qwe",
                    detail = "123",
                    thumb = "qwe",
                    status = true,
                    datePublic = DateTime.Parse("1989-1-11"),
                    CategoryId = 2,
                },

                new Blog
                {
                    title = "Test update",
                    shortDetail = "qwe",
                    detail = "123",
                    thumb = "qwe",
                    status = false,
                    datePublic = DateTime.Parse("1989-1-11"),
                    CategoryId = 1,
                }
            );
        }
    }
}
