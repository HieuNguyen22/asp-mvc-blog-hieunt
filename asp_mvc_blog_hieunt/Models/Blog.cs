﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace asp_mvc_blog_hieunt.Models
{
    public class Blog
    {
        public int ID { get; set; }

        [Display(Name = "Tiêu đề")]
        [StringLength(30, MinimumLength = 3)]
        public string title { get; set; }

        [Display(Name = "Mô tả ngắn"), DataType(DataType.MultilineText)]
        [StringLength(60, MinimumLength = 3)]
        public string shortDetail { get; set; }

        [Display(Name = "Chi tiết"), DataType(DataType.MultilineText)]
        [StringLength(300, MinimumLength = 3)]
        public string detail { get; set; }

        [Display(Name = "Hình ảnh")]
        public string thumb { get; set; }

        [Display(Name = "Trạng thái")]
        public bool status { get; set; }

        [Display(Name = "Ngày tạo")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime datePublic { get; set; }

        [Display(Name = "Category ID")]
        public int CategoryId { get; set; }

        [Display(Name = "Loại")]
        public virtual Category category { get; set; }

        [Display(Name = "Vị trí")]
        public virtual IList<Position> Positions { get; set; }
    }
    public class BlogDBContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Position> Positions { get; set; }
    }

}